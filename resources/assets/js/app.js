
require('./bootstrap');
// //FONT
import fontawesome from '@fortawesome/fontawesome';

import fas from '@fortawesome/fontawesome-free-solid';
import fab from '@fortawesome/fontawesome-free-brands';
import far from '@fortawesome/fontawesome-free-regular';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
fontawesome.library.add(fab,far);
fontawesome.library.add(fas)

global.toastr = require('toastr');
toastr.options.closeMethod = 'fadeOut';
toastr.options.closeDuration = 300;
toastr.options.closeEasing = 'swing';

Vue.component('font-awesome-icon', FontAwesomeIcon);


//VUE Y RUTAS
import Vue from 'vue';
import VueRouter from 'vue-router';
import {routes} from './routes';
import MainApp from './components/MainApp.vue';
import VueAuth from '@websanova/vue-auth'

import VueAxios from 'vue-axios';
Vue.use(VueRouter);

Vue.use(VueAxios, axios);

axios.defaults.baseURL = '/';


const router = new VueRouter({
    routes,
    mode: 'history'
});

Vue.router = router;
Vue.use(VueAuth, {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    logoutData: {url: 'api/auth/logout', method: 'POST', redirect: '/login', makeRequest: false},
    loginData: {url: 'api/auth/login', method: 'POST', redirect: '/', fetchUser: true},
    fetchData: {url: 'api/auth/user', method: 'GET', enabled: true},
    refreshData: {url: 'api/auth/refresh', method: 'GET', enabled: true, interval: 30},
    notFoundRedirect: {path:'/404'},
    forbiddenRedirect: {path: '/403'},
    rolesVar: 'roles'
});

export const bus = new Vue();

const app = new Vue({
    el: '#app',
    router,
    components:{
        MainApp
    }
});
app.router = router;