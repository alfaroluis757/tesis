<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        //'user_id' => $faker->numberBetween($min = 4, $max = 19),
        'contenido' => $faker->realText($maxNbChars = 200),
        'likes' => 0
    ];
});
