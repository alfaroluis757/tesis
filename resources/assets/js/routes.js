/*Auth */ 
import Login from './components/Auth/Login.vue';
import App from './components/Auth/App.vue';
import Register from './components/Auth/Register.vue';
import Forgot from './components/Auth/forgot.vue';
import Reset from './components/Auth/reset.vue';
import NotFound from './components/Auth/NotFound.vue'


/*Bibloteca */
import Biblioteca from './views/Busqueda/biblioteca.vue';

/*Profesor */
import BibliotecaLista from './views/Profesor/lista-biblioteca.vue';

/* Estudiante Y Profesor */
import RedSocial from './views/redsocial/redsocial.vue';
import PublicacionExtendido from './views/redsocial/publicacion-extendido.vue';

/*Todos */ 
import User from './views/user/user.vue';


/*Administrador Del Colegio */ 
import AdminColegio from './views/AdministradorColegio/Administrador';

/*Administrador del sistema */ 

import AdminSistema from './views/Administrador/Administrador';

export const routes=[
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false,
        }
    }, 
    //prueba
    {
        path: '/loginn',
        name: 'loginn',
        component: App,
        meta: {
            auth: true,
        }
    },{
        path: '/forgot',
        name: 'forgot',
        component: Forgot,
        meta: {
            auth: false,
        }
    },{
        path: '/404',
        name: '404',
        component: NotFound,
       
    },
    {
        path:'/biblioteca/busqueda',
        component: Biblioteca,
        meta: {
            auth: {
                roles: ['Profesor','Estudiante'],
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },{
        path:'/biblioteca/busqueda/:id',
        name:'compartir',
        component: Biblioteca,
        meta: {
            auth: {
                roles: ['Profesor','Estudiante'],
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },
    {
        path:'/biblioteca/lista',
        name:'BibliotecaCrud',
        component: BibliotecaLista,
        meta: {
            auth: {
                roles: 'Profesor',
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },
    {
        path:'/redsocial',
        name:'RedSocial',
        component: RedSocial,
        meta: {
            auth: {
                roles: ['Profesor','Estudiante'],
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },
    {
        path:'/redsocial/publicacion',
        component: PublicacionExtendido
    },
    {
        path:'/user',
        component: User
    }, {
        path:'/admin/colegio',
        name:'AdminColegio',
        component: AdminColegio,
        meta: {
            auth: {
                roles: 'Administrador de Colegio',
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },{
        path:'/admin/sistema',
        name:'AdminSistema',
        component: AdminSistema,
        meta: {
            auth: {
                roles: 'Administrador del Sistema',
                redirect: '/login',
                forbiddenRedirect: '/login'
            },
        }
    },
    {
        path:'/*',
        component: NotFound
    }
];
