<?php

use Faker\Generator as Faker;

$factory->define(App\Comentario::class, function (Faker $faker) {
    $user = App\User::where('id', '>=', 4)->get()->random();
    $post = App\Post::where('colegio_id', $user->colegio_id)->get()->random();
    return [
        'user_id' => $user->id,
        'post_id' => $post->id,
        'contenido' => $faker->realText($maxNbChars = 70)
    ];
});
