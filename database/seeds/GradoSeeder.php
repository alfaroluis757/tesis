<?php

use Illuminate\Database\Seeder;

class GradoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Grado::class)->create([
            'grado' => 'Primer Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Segundo Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Tercer Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Cuarto Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Quinto Año'
        ]);
    }
}
