<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'colegio_id', 'contenido', 'likes'
    ];

    //UNO A MUCHOS
    public function comentarios()
    {
        return $this->hasMany('App\Comentario');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function userPostLikes()
    {
        return $this->hasMany('App\UserPostLike');
    }

    //MUCHOS A UNO

    public function usuarios()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function colegios()
    {
        return $this->belongsTo('App\Colegio','colegio_id');
    }

}
