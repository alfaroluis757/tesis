<?php

use Faker\Generator as Faker;

$factory->define(App\BibliotecaPost::class, function (Faker $faker) {
    return [
        // 'colegio_id' => $faker->numberBetween($min = 1, $max = 2),
        // 'user_id' => $faker->numberBetween($min = 4, $max = 9),
        'privacidad' => 1,
        'titulo' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'descripcion' => $faker->realText($maxNbChars = 100),
        'url' => $faker->url,
        'likes' => 0,
        'grado_id' => $faker->numberBetween($min = 1, $max = 5),
        'materia_id' => $faker->numberBetween($min = 1, $max = 6),
        'otra_materia_id' => null,
        'archivo' => false
    ];
});
