<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        $this->call([
            ColegioSeeder::class,
            RolSeeder::class,
            GradoSeeder::class,
            MateriaSeeder::class,
        ]);
        */
        
        factory(App\Colegio::class)->create([
            'colegio' => 'UE El Primero',
        ]);

        factory(App\Colegio::class)->create([
            'colegio' => 'UE Segundo',
        ]);

        factory(App\Rol::class)->create([
            'rol' => 'Administrador del Sistema'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Administrador de Colegio'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Profesor'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Estudiante'
        ]);


        factory(App\Grado::class)->create([
            'grado' => 'Primer Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Segundo Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Tercer Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Cuarto Año'
        ]);
        factory(App\Grado::class)->create([
            'grado' => 'Quinto Año'
        ]);


        $materia = array("Ingles", "Matematica", "Lenguas", "Historia", "Quimica", "Fisica");
        for($i=0; $i<count($materia); $i++){
            factory(App\Materia::class)->create([
                'nombre' => $materia[$i]
            ]);
        }

        factory(App\User::class)->create([
            'colegio_id' => null,
            'cedula' => '123456',
            'nombre' => 'admin',
            'apellido' => 'istrador',
            'username' => 'administrador',
            'grado_id' => null,
            'email' => 'administrador@mail.com',
            'postRS' => 1,
            'postBV' => 1,
            'rol_id' => 1
        ]);

        factory(App\User::class)->create([
            'colegio_id' => null,
            'cedula' => '12345343456',
            'nombre' => 'luis',
            'apellido' => 'luis',
            'username' => 'luis',
            'grado_id' => null,
            'email' => 'alfaroluis757@gmail.com',
            'postRS' => 1,
            'postBV' => 1,
            'rol_id' => 1
        ]);

        factory(App\User::class)->create([
            'colegio_id' => 1,
            'grado_id' => null,
            'rol_id' => 2
        ]);

        factory(App\User::class)->create([
            'colegio_id' => 2,
            'grado_id' => null,
            'rol_id' => 2
        ]);

        factory(App\User::class, 3)->create([
            'colegio_id' => 1,
            'grado_id' => null,
            'rol_id' => 3
        ]);

     /*   factory(App\BibliotecaPost::class, 6)->create([
            'colegio_id' => 1,
            'user_id' => rand(4, 6),
        ]);
*/
        factory(App\User::class, 3)->create([
            'colegio_id' => 2,
            'grado_id' => null,
            'rol_id' => 3
        ]);

       /* factory(App\BibliotecaPost::class, 6)->create([
            'colegio_id' => 2,
            'user_id' => rand(7, 9),
        ]);*/

        factory(App\User::class, 10)->create();

        for($i = 0; $i < 10; $i++){
            $user = App\User::find(10 + $i);

            factory(App\Post::class, rand(1, 3))->create([
                'user_id' => $user->id,
                'colegio_id' => $user->colegio_id
            ]);
        }

        factory(App\Comentario::class, 20)->create();

    }
}
