<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $fillable = [
        'grado'
    ];

    public function bibliotecaPosts()
    {
        return $this->hasMany('App\BibliotecaPost');
    }
    public function usuarios()
    {
        return $this->hasMany('App\User');
    }
}
