<?php
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;


Route::get('password/email',  [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

Route::get('/password/reset/{token}',  [ResetPasswordController::class, 'showResetForm'])->name('password.reset');

Route::post('password/reset',  [ResetPasswordController::class, 'reset'])->name('password.request');

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/home', function () {
    return redirect('/login');
});
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');
