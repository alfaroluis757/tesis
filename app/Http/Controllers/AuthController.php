<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Validator;
use DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','recover']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {   
        $credentials = request(['email', 'password']);
        $user = User::where('email','=',$credentials['email'])->get();
        if($user->isEmpty()){
            return response([
                'status' => 'error',
                'error' => 'invalid.email',
                'message' => 'No estas Registrado'
            ], 400);
        }
        if (! $token = auth()->attempt($credentials)) {
            return response([
                'status' => 'error',
                'error' => 'invalid.password',
                'message' => 'Contraseña Invalida'
            ], 400);
        }

        $tokenn = $this->respondWithToken($token);
        return response([
            'status' => 'success',
            'token' =>  $tokenn ,
            'user' => $this->user()
        ],200)
        ->header('Authorization', $token);

    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {   
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out'],200);
        
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {   
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ])->header('Authorization', $token);
    }

    public function user()
    {
        $user = User::find(Auth::user()->id);
        $user->roles = $user->rol->rol;
        return response([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function recover(Request $request)
    {   
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "No estas Registrado.";
            return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 200);
        }
        try {
            Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject('Your Password Reset Link');
            });
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json([
            'success' => true, 'data'=> ['msg'=> 'Se ha enviado un correo a tu direccion de email.']
        ]);
    }

}