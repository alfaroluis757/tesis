<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
    protected $fillable = [
        'colegio', 'foto',
    ];


    //UNO A MUCHOS
    public function bibliotecaPosts()
    {
        return $this->hasMany('App\BibliotecaPost');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function otrasMaterias()
    {
        return $this->hasMany('App\OtraMateria');
    }

    public function usuario()
    {
        return $this->hasMany('App\User');
    }
}
