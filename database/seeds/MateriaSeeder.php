<?php

use Illuminate\Database\Seeder;

class MateriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materia = array("Ingles", "Matematica", "Lenguas", "Historia", "Quimica", "Fisica");
        for($i=0; $i<count($materia); $i++){
            factory(App\Materia::class)->create([
                'nombre' => $materia[$i]
            ]);
        }
    }
}
