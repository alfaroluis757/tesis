<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;


    protected $fillable = [
        'colegio_id', 'cedula', 'nombre','apellido','username','imagen','grado_id','postRS','postBV','email', 'password', 'rol_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //UNO A MUCHOS
    public function bibliotecaPosts()
    {
        return $this->hasMany('App\BibliotecaPost');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Comentario');
    }
    public function reports()
    {
        return $this->hasMany('App\Report');
    }
    public function userPostLikes()
    {
        return $this->hasMany('App\UserPostLike');
    }


    //MUCHOS A UNO
    public function colegio()
    {
        return $this->belongsTo('App\Colegio','colegio_id');
    }
    public function grados()
    {
        return $this->belongsTo('App\Grado','grado_id');
    }
    public function rol()
    {
        return $this->belongsTo('App\Rol','rol_id');
    }

    //AUTH


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


}
