<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'user_id', 'post_id', 'contenido',
    ];

    //Muchos A uno

    public function usuarios()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function posts()
    {
        return $this->belongsTo('App\Post','post_id');
    }


}
