<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//AUTH 


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::get('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@user');
    
    
});

Route::post('recover', 'AuthController@recover');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Biblioteca

Route::get('materias','BibliotecaController@obtenertodo');
Route::post('busqueda','BibliotecaController@busqueda');
Route::post('likeBibli','BibliotecaController@like');
Route::post('dislikeBibli','BibliotecaController@dislike');
Route::post('buscarbibli','BibliotecaController@buscarbibli');
//BIBLIOTECA CRUD
Route::post('/agregarPostBibli','BibliotecaController@store');
Route::get('/obtenerinfo','BibliotecaController@obtenerinfo');


//CRUD BIBLIOTECA
Route::post('/eliminarbiblioteca','BibliotecaController@eliminar');

