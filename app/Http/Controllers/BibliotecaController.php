<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use Validator;


use App\Materia;
use App\OtraMateria;
use App\Grado;
use App\User;
use App\colegio;
use App\BibliotecaPost;
use App\UserPostLike;
class BibliotecaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('biblioteca.biblioteca');
    }
    
    
    //BUSQUEDA DE BIBLIOTECA
    public function obtenertodo(){


        //materias
        $materias = Materia::all();
        $materiasAdicionales = OtraMateria::where('colegio_id','=',Auth::User()->colegio_id)->get();
        $materias = $materias->concat($materiasAdicionales);
        
        //grados
        $grados = Grado::all();

        //profesores
        $Usuario = User::find(Auth::User()->id);
        
        $profesores = User::where('colegio_id','=',$Usuario->colegio_id)
                                ->where('rol_id','=','3')
                                ->get();
        
                                
        $todo =[
            'materias'=>$materias,
            'materiasA' =>$materiasAdicionales,
            'grados' =>$grados,
            'profesores' =>$profesores
        ];

        return $todo;

    }

    public function Busqueda(Request $request){

        $titulo = $request->get('titulo');
        $profesor = $request->get('profesor');
        $grado = $request->get('anio');
        $materia = Materia::where('nombre','=',$request->materia)->get();
        $adicional = OtraMateria::where('nombre','=',$request->materia)->get();
        $materia_id="";
        $adicional_id="";
        $adicionalb = false;
            if($adicional->isNotEmpty()){
                $adicional_id = $adicional[0]->id;
                $adicionalb= true;
            }
        
           
            if($materia->isNotEmpty())
                $materia_id = $materia[0]->id;
        
       
        $Usuario = User::find(Auth::User()->id);
        $bibliotecas = BibliotecaPost::orderBy('updated_at','DESC')
                                    ->titulo($titulo)
                                    ->profesor($profesor)
                                    ->grado($grado)
                                    ->materia($materia_id)
                                    ->adicional($adicional_id)
                                    ->where('colegio_id','=',$Usuario->colegio_id)
                                    ->get();
                                    
   
        foreach($bibliotecas as $key=>$biblioteca){
            $biblioteca->like= false;
            $biblioteca->usuarios;
            $biblioteca->materias;
            $biblioteca->grados;
            $biblioteca->otrasMaterias;
            $biblioteca->userPostLikes;
            foreach($biblioteca->userPostLikes as $like){
                if($like->user_id == $Usuario->id){
                    $biblioteca->like = true;
                    break;
                }
            }
        }

        //$biblioteca = $biblioteca->concat([$adicionalb]);
       
        return response()->json([
            "biblioteca" => $bibliotecas
        ],200);
    }

    public function like(Request $request){
      
        $like = new UserPostLike();
        $like->user_id = Auth::User()->id;
        $like->biblioteca_post_id= $request->id_post;
        $like->save();

        $post = BibliotecaPost::find($request->id_post);
        $post->likes= $post->likes+1;
        $post->update();
        
        return response()->json([
            "like" => true,
            "post" => $post
        ],200);
    }

    public function dislike(Request $request){
      
        
        $like = UserPostLike::where('user_id','=',Auth::User()->id)
                            ->where('biblioteca_post_id','=',$request->id_post)
                            ->delete();
                            
      
        $post = BibliotecaPost::find($request->id_post);
        $post->likes= $post->likes-1;
        $post->update();
        
        return response()->json([
            "like" => false,
            "post" => $post
        ],200);
    }

    public function buscarbibli(Request $request){
        $biblioteca = BibliotecaPost::find($request->id);
        $biblioteca->materias;
        $biblioteca->grados;  
        $biblioteca->usuarios;
        $biblioteca->like= false;
        $biblioteca->userPostLikes;
        $Usuario = User::find(Auth::User()->id);
            foreach($biblioteca->userPostLikes as $like){
                if($like->user_id == $Usuario->id){
                    $biblioteca->like = true;
                }
        }
        return response()->json([
            "biblioteca" => $biblioteca,
        ],200);
    }



    public function store(Request $request)
    {   
        
        $bibli = new BibliotecaPost();
        $bibli->colegio_id = Auth::User()->colegio_id;
        $bibli->user_id = Auth::User()->id;
        $bibli->privacidad = $request->privacidad;
        $bibli->titulo = $request->titulo;
        $bibli->descripcion = $request->descripcion;
        
        $bibli->likes = 0;
        $bibli->grado_id = $request->grado;
        if($request->materia != ""){
            $bibli->materia_id = $request->materia;
            $bibli->otra_materia_id = null;
        }else{
            $bibli->materia_id = null;
            $bibli->otra_materia_id = $request->adicional;
        }
        if($request->tipoURL=="URL"){
            $bibli->url = $request->url;
            $bibli->archivo = false;
            $bibli->tipo = null;
        }else{
            if($request->hasfile('archivo')){
               
                $rules = ['archivo' => 'required|mimes:pdf|max:5000',];
                $messages = [
                    'required' => 'El Archivo es requerido',
                    'max' => 'El máximo permitido es de 5 MB',
                ];
                $validator = Validator::make($request->all(), $rules, $messages);
               
                if ($validator->fails()){
                    return response()->json([
                        'errores' => $validator
                    ],200);
                }else{
                    $bibli->archivo = true;
                    
                    $file = $request->file('archivo');
                    $archivoName = date("d")."-".date("m")."-".date("Y").str_random(20)."-".date("h").'.'.$file->getClientOriginalExtension();
                    $bibli->tipo = $file->getClientOriginalExtension();
                    $file->move(public_path().'/storage/',$archivoName);
                    $bibli->url = '/storage/'.$archivoName;
                }
            }
        }
       
       
        $bibli->save();
        $Usuario = User::find(Auth::User()->id);
        $bibli->like= false;
        $bibli->usuarios;
        $bibli->materias;
        $bibli->grados;
        $bibli->otrasMaterias;
        $bibli->userPostLikes;
        foreach($bibli->userPostLikes as $like){
            if($like->user_id == $Usuario->id){
                $bibli->like = true;
            }
        }
        $mensajes = ["mensaje" => "Post Agregado Correctamente"];
        return response()->json([
            "bibli" => $bibli,
            "mensajes" => $mensajes
        ]);
    }

    public function obtenerinfo(){
     //   return response()->json(["hola" => Auth::User()->id]);

        //materias
        $materias = Materia::all();
        $materiasAdicionales = OtraMateria::where('colegio_id','=',Auth::User()->colegio_id)->get();
       // $materias = $materias->concat($materiasAdicionales);
        $bibliotecas = BibliotecaPost::where('user_id','=',Auth::User()->id)->orderBy('updated_at','DESC')->get();
        $Usuario = User::find(Auth::User()->id);
        foreach($bibliotecas as $key=>$biblioteca){
            $biblioteca->like= false;
            $biblioteca->usuarios;
            $biblioteca->materias;
            $biblioteca->grados;
            $biblioteca->otrasMaterias;
            $biblioteca->userPostLikes;
            foreach($biblioteca->userPostLikes as $like){
                if($like->user_id == $Usuario->id){
                    $biblioteca->like = true;
                }
            }
        }

        //grados
        $grados = Grado::all();

        return response()->json([
            "materias" => $materias,
            "adicionales" => $materiasAdicionales,
            "grados" => $grados,
            "bibliotecas" => $bibliotecas
        ],200);
    }

    //CRUDDD BIBLIOTECA 


    public function eliminar(Request $request){
        BibliotecaPost::destroy($request->id);
        return response()->json([
            "mensaje" => "Se ha Eliminado Correctamente"
        ]);
    }

    public function updateBibli(Request $request){

        $bibli = BibliotecaPost::find($request->id);
        $bibli->privacidad = $request->privacidad;
        $bibli->titulo = $request->titulo;
        $bibli->descripcion = $request->descripcion;
        $bibli->likes = 0;
        $bibli->grado_id = $request->grado;
        if($request->materia != ""){
            $bibli->materia_id = $request->materia;
            $bibli->otra_materia_id = null;
        }else{
            $bibli->materia_id = null;
            $bibli->otra_materia_id = $request->adicional;
        }
        if($request->tipoURL=="URL"){
            $bibli->url = $request->url;
        }else{
          
            if($request->hasfile('archivo')){
               
                $rules = ['archivo' => 'required|mimes:pdf|max:5000',];
                $messages = [
                    'required' => 'El Archivo es requerido',
                    'max' => 'El máximo permitido es de 5 MB',
                ];
                $validator = Validator::make($request->all(), $rules, $messages);
               
                if ($validator->fails()){
                    return response()->json([
                        'errores' => $validator
                    ],200);
                }else{
                    $file = $request->file('archivo');
                    $archivoName = date("d")."-".date("m")."-".date("Y").str_random(20)."-".date("h").'.'.$file->getClientOriginalExtension();
                    $file->move(public_path().'/storage/',$archivoName);
                    $bibli->url = '/storage/'.$archivoName;
                }
            }
        }
        $bibli->update();
        $Usuario = User::find(Auth::User()->id);
        $bibli->like= false;
        $bibli->usuarios;
        $bibli->materias;
        $bibli->grados;
        $bibli->otrasMaterias;
        $bibli->userPostLikes;
        foreach($bibli->userPostLikes as $like){
            if($like->user_id == $Usuario->id){
                $bibli->like = true;
            }
        }
        return response()->json([
            "bibli" => $bibli
        ]);
    }

}
