<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPostLike extends Model
{
    protected $fillable = [
        'user_id', 'post_id' ,'biblioteca_post_id'
    ];

    public function usuarios()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function posts()
    {
        return $this->belongsTo('App\Post','post_id');
    }

    public function bibliotecaPosts()
    {
        return $this->belongsTo('App\BibliotecaPost','biblioteca_post_id');
    }
}
