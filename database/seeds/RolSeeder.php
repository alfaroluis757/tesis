<?php

use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Rol::class)->create([
            'rol' => 'Administrador del Sistema'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Administrador de Colegio'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Profesor'
        ]);
        factory(App\Rol::class)->create([
            'rol' => 'Estudiante'
        ]);
    }
}
