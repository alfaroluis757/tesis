<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    $nombre = $faker->firstName;
    $apellido = $faker->lastName;
    $username = $nombre.$apellido;
    $email = $username."@mail.com";
    return [
        'colegio_id' => $faker->numberBetween($min = 1, $max = 2),
        'cedula' => $faker->unique()->numberBetween($min = 1000000, $max = 40000000),
        'nombre' => $nombre,
        'apellido' => $apellido,
        'username' => $username,
        'imagen' => "default",
        'grado_id' => $faker->numberBetween($min = 1, $max = 5),
        'postRS' => 1,
        'postBV' => 1,
        'email' => $email,
        'password' => bcrypt(123456),
        'remember_token' => str_random(10),
        'rol_id' => 4
    ];
});
