<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BibliotecaPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biblioteca_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('colegio_id')->unsigned();
            $table->foreign('colegio_id')->references('id')->on('colegios');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('privacidad')->unsigned();
            $table->integer('grado_id')->unsigned()->nullable();
            $table->foreign('grado_id')->references('id')->on('grados');
            $table->integer('materia_id')->unsigned()->nullable();
            $table->foreign('materia_id')->references('id')->on('materias');
            $table->integer('otra_materia_id')->unsigned()->nullable();
            $table->foreign('otra_materia_id')->references('id')->on('otra_materias');
            $table->string('titulo');
            $table->text('descripcion');
            $table->text('url');
            $table->string('tipo')->nullable();
            $table->integer('likes');
            $table->boolean('archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
