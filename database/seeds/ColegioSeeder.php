<?php

use Illuminate\Database\Seeder;

class ColegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Colegio::class)->create([
            'colegio' => 'UE El Primero',
        ]);

        factory(App\Colegio::class)->create([
            'colegio' => 'UE Segundo',
        ]);
    }
}
