<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtraMateria extends Model
{
    protected $fillable = [
        'nombre', 'colegio_id'
    ];

    public function bibliotecaPosts()
    {
        return $this->hasMany('App\BibliotecaPost');
    }

    public function colegios()
    {
        return $this->belongsTo('App\Colegio','colegio_id');
    }
}
