<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BibliotecaPost extends Model
{
    protected $fillable = [
        'colegio_id', 'user_id', 'privacidad','titulo','descripcion','url','likes', 'grado_id', 'materia_id', 'otra_materia_id'
    ];

    //UNO A MUCHOS
    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function userPostLikes()
    {
        return $this->hasMany('App\UserPostLike');
    }

    //MUCHOS A UNO

    public function usuarios()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function colegios()
    {
        return $this->belongsTo('App\Colegio','colegio_id');
    }

    public function materias()
    {
        return $this->belongsTo('App\Materia','materia_id');
    }

    public function otrasMaterias()
    {
        return $this->belongsTo('App\OtraMateria','otra_materia_id');
    }

    public function grados()
    {
        return $this->belongsTo('App\Grado','grado_id');
    }


    public function scopeTitulo($query, $titulo){
        if($titulo)
            return $query->where('titulo','LIKE',"%$titulo%");
    }
    public function scopeDescripcion($query, $descripcion){
        if($descripcion)
            return $query->where('descripcion','LIKE',"%$descripcion%");
    }

    public function scopeProfesor($query, $profesor){
        if($profesor!='')
            return $query->where('user_id','=',$profesor);
    }
    public function scopeGrado($query,$grado){
        if($grado!='')
            return $query->where('grado_id','=',$grado);
    }
    public function scopeMateria($query,$materia_id){
        if($materia_id!='')
            return $query->where('materia_id','=',$materia_id);
    }
    public function scopeAdicional($query,$adicional_id){
        if($adicional_id!='')
            return $query->where('otra_materia_id','=',$adicional_id);
    }


}
